<!-- Fixes Navigation Bar with drop down menu
======================================================-->

<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="http://esparaquia.com" class="navbar-brand">esparaquia.com</a>
        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="page1.php">Project1</a></li>
                <li><a href="page2.php">Project2</a></li>
            </ul>
        </div>

    </div>
</div>
