<head>
    <title>news.esparaquia.com</title>
    
    <link type="text/css" rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css" />
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="node_modules/datatables.net-bs/css/dataTables.bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" />
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/datatables.net/js/jquery.dataTables.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>  
</head>