<!DOCTYPE html>
<html>
    <?php include 'layout/header.php' ;?>
    <?php include 'layout/navbar.php' ;?>
    <body>
        <br/><br/><br/><br/>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#table').dataTable({
                    paging: false
                });
            })
        </script>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron text-center">
                        <p>Los se&ntilde;ores mineros, Sirilo, Rufino, Fortunato y Cabeza de Vaca, 
                            los proveedores de ayuda m&aacute;gica a los traviesos se 
                            enorgullecen en presentar la tabla del Merodeador: 
                            
                        <p>
                            El proposito de esta labor, es presentar un sondeo de las noticias diarias a trav&eacute;s de 31 periodicos mexicanos obtenidos de 
                            <a href='http://oem.com.mx/oem/'>http://oem.com.mx/oem/</a></p>
                        </p>
                    </div>
                </div>
                <div class="col-md-12">
                    <table id='table' class='table-responsive'>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Encabezado</th>
                            <th>Descripcion</th>
                            <th>Sitio</th>
                        </tr>
                    </thead>
                    <tbody>
	                        <?php include_once 'src/soup.php' ;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
    <?php include 'layout/footer.php' ;?>
</html>

